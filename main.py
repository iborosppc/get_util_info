#!/usr/bin/python3.5

# DOWNLOAD Testbed file from KICK FastPods

import os
import yaml


def get_download_content():
    user_dir = os.path.expanduser('~')
    os.chdir(user_dir+"/Downloads")
    files = sorted(os.listdir(os.getcwd()), key=os.path.getmtime, reverse=True)
    return files


def get_desired_yml(files):
    return next(file for file in files if str(file).endswith(("yaml", "yml")))


def open_file(file):
    with open(file) as opf:
        yml_file = yaml.load(opf)
        devices = yml_file["devices"]
        topology = yml_file["topology"]
        get_devices(devices, topology)


def get_devices(devices, topology):
    file = create_text()
    for dev in devices:
        user = devices[dev]["connections"]["management"]["user"]
        port = str(devices[dev]["connections"]["management"]["port"])
        alias = devices[dev]["alias"]
        ip = get_topology(topology, dev)
        if str(dev).startswith("sensor"):
            passwd = devices[dev]["connections"]["management"]["password"]
            credential = "Credentials:",  user, " ", passwd
            ssh_con = "ssh ", user,"@",ip, " -p ", port
            info = alias, credential, ssh_con
            write(file, info)
        elif str(dev).startswith("fmc"):
            w_ip = devices[dev]['connections']['web']['ip']
            w_port = devices[dev]['connections']['web']['port']
            web_url = "web URL:", w_ip,":",w_port
            f_ssh = "ssh ", user, '@',ip, ' -p ', port
            f_info = alias, web_url, f_ssh
            write(file, f_info)
    file.close()


def create_text():
    user_dir = os.path.expanduser('~')
    file = open(user_dir + '/Desktop/PATH.txt', 'w')
    return file


def write(file, info):
    for t in info:
        file.writelines(''.join(str(s) for s in t) + '\n')


def get_topology(topology, dev):
    for t_dev in topology:
        if t_dev == dev:
            ip = topology[t_dev]['interfaces']['Diagnostic1/1']['ipv4']
            ip = ip[:ip.find("/")]
            return ip


def execute_scr():
    files = get_download_content()
    file = get_desired_yml(files)
    open_file(file)


if __name__ == "__main__":
    execute_scr()
